console.log("Hello World from s15!");

/*Mini-Activity (Variables)*/

let numString1 = "5";
let numString2 = "10";
let num1 = 4;
let num2 = 6;
let num3 = 1.5;
let num4 = .5;

//Lesson Proper

	//Operators - allows our programming languages to execute an operation or evaluation. In JS, when we use an operator, a value is returned. For mathematical operators, a number is returned. For comparison operators, a boolean is returned. These returned values can be saved in a variable which we can use later.

		//Mathematical Operators (+,-,/,*,%)

			console.log(num1 + num2);//10

			//With the use of +, we can return a value from addition between two number types. We can also save the return value in a variable.

			let sum1 = num1 + num2;
			console.log(sum1);//10

			let numString3 = numString1 + sum1;
			console.log(numString3);//510. It results to concatenation because at least one of the operators is a string. Javascript automatically converted the number to a string because it detected that at least 1 of the operands is a string.

			/*
				number + number = proper mathematical addition, results to sum

				string + number = string. Because instead, concatenation happens.
			*/

			let sampleStr = "Charles";
			console.log(sampleStr + num2);//Charles6

		//Substraction Operator (-)

			//Allows us to subtract the operands and result to a difference.
			//It returns a number that we can save in a variable.

			let difference1 = num1 - num3;
			console.log(difference1);

			/*
				For subtraction operator, the string is converted back to a number and then subtracted.
			*/

			let difference2 = numString2 - num2;
			console.log(difference2);

			let sampleStr2 = "Joseph";

			let difference3 = sampleStr2 - num1;
			console.log(difference3);//NaN - not a number. "Joseph" was converted to a number and resulted to NaN, NaN - number will result to NaN.

			let difference4 = numString2 - numString1;
			console.log(difference4);//5 - number. Both numeric strings were converted into numbers and then subtracted.

			/*Mini-Activity (Function-Subtract)*/

			function subtract(num1,num2){
				return num1 - num2;
			}

			let difference5 = subtract(24,7);
			console.log(difference5);

		//Multiplication Operator (*)

			//Multiply both operands and return the product as value.
			//Convert any string to number before multiplying.

			let product1 = num1 * num2;
			console.log(product1);//24 - number

			let product2 = numString1 * numString2;
			console.log(product2);//50 - number

			/*
				Much like subtraction, string multiplied with a string, we convert the strings into numbers then multiply. It will result to proper multiplication.
			*/

		//Division Operator (/)

			//Divider right operand from the left operand.
				//leftOperand/rightOperand
			//Converts strings into numbers then divide.

			let quotient1 = num2 / num4;
			console.log(quotient1);//12 - number

			let quotient2 = numString2 / numString1;
			console.log(quotient2);//2 - number. Both strings converted into numbers before division.

		/*Mini-Activity (Multiply and Divide)*/

			function multiply(num1,num2){
				return num1 * num2;
			}

			let product3 = multiply(4,5);
			console.log(product3);

			function divide(num1,num2){
				return num1 / num2;
			}

			let quotient3 = divide(20,5);
			console.log(quotient3);

		/*Multiplying or Dividing by 0*/

		console.log(product3*0);//0 - any number multiplied by 0 is 0.
		console.log(quotient3/0);//infinity - special type of number meaning infinity. Because we cannot divide a number by 0.

		/*Modulo Operator (%)*/

			//Modulo is the remainder of a division operator.
			//5/5 = 1 = 0
			console.log(5%5);//0
			//leftOperand%rightOperand = modulo or the remainder
			console.log(25%6);//1
			//25/6 = remainder is 1
			console.log(50%2);//0
			//50/2 = 25 = remainder is 0

			let modulo1 = 5%6;
			//We were able to save the value returned by the modulo operation which is 5.
			console.log(modulo1);

	//Assignment Operators

		//Basic Assignment Operator (=)

			//It allows us to assign a value to the left operand.
			//It also allows us to assign an initial value to a variable.

			/*
				leftOperand = rightOperand
			*/

			let variable = "initial value";
			variable = "new value";
			console.log(variable);//new value

			let sample1 = "sample value";
			variable = sample1;
			console.log(variable);//sample value
			console.log(sample1);//sample value

			let sample2 = "new sample value";
			variable = sample2;
			console.log(variable);//new sample value
			console.log(sample2);//the same as initial(new sample value)


			/*a constant's value cannot be updated or re-assigned.*/
			/*const pi = 3.1416;
			pi = 5000;
			console.log(pi);*/

			//Note: Do not add a basic assignment operator directly into the return statement

			function displayDetails(name,password,age){
				//Error:
				/*return = {
					name: name,
					password: password,
					age: age
				}*/

				//Correct:
				let details = {
					name: name,
					password: password,
					age: age
				}
				return details
			}

		//Addition Assignment Operator (+=)
			//The result of the addition operation is re-assignes as the value of the left operand.

			let sum2 = 10;
			//10 + 20 = 30
			//sum2 = 30 (result of addition)
			sum2 += 20;
			console.log(sum2);

			let sum3 = 5;
			//5 + 5 = 10
			//sum3 = 10
			sum3 += 5;
			console.log(sum3);//10

			//When using addition assignment operator, keep in mind that the left operand should be a variable.
			/*console.log(5+=5);*/

			let sum4 = 30;
			//30 + "Curry" = 30 will be converted into a string and will be concatenated with the string "Curry"
			//sum4 = "30Curry"
			sum4 += "Curry";
			console.log(sum4);

			let sum5 = "50";
			//"50" + "50" = Since both are strings, it concatenated to "5050"
			//sum5 = "5050"
			sum5 += "50";
			console.log(sum5);

			let fullName = "Wardell";
			let name1 = "Stephen";
			//"Wardell" + "Stephen" = "WardellStephen"
			//fullName = "WardellStephen"
			fullName += name1;
			console.log(fullName);//"WardellStephen"
			fullName += "Curry";
			//"WardellStephen" + "Curry" = "WardellStephenCurry"
			//fullName = "WardellStephenCurry"
			console.log(fullName);//"WardellStephenCurry"
			fullName += "II";
			console.log(fullName);

		//Subtraction Assignment Operator (-=)

			//The result of subtraction between both operands will be saved as the value of the left operand.

			let numSample = 50;
			//50 - 10 = 40
			//numSample = 40 (result of subtraction)
			numSample -= 10;
			console.log(numSample);

			let numberString = "100";
			let numberString2 = "50";
			//"100" - "50" = both strings are converted to numbers and subtracted = 100 - 50 = 50
			//numberString = 50
			numberString -= numberString2;
			console.log(numberString);//50

			let text = "ChickenDinner";
			//"ChickenDinner" - "Dinner". Both converted to numbers but since they are alphanumeric/words, it results to NaN or not a number.
			//NaN - NaN = NaN
			//text = NaN
			text -= "Dinner";
			console.log(text);//NaN

		//Multiplication Assignment Operator (*=)

			//The result of multiplication between the operands will be re-assigned as the value of the left operand.

			let sampleNum1 = 3;
			let sampleNum2 = 4;
			//3 * 4 = 12
			//sampleNum1 = 12
			sampleNum1 *= sampleNum2;
			console.log(sampleNum1);//12
			console.log(sampleNum2);//4

			let sampleNum3 = 5;
			let sampleNum4 = "6";
			//5 * "6" = conversion to numbers = 5 * 6 = 30
			//sampleNum3 = 30
			sampleNum3 *= sampleNum4;
			console.log(sampleNum3);

		//Division Assignment Operator (/=)

			//The result of division between both operands will be re-assigned as the value of the left operand.

			let sampleNum5 = 70;
			let sampleNum6 = 10;
			//70/10 = 7
			//sampleNum5 = 7
			sampleNum5 /= sampleNum6;
			console.log(sampleNum5);//7

			let sampleNum7 = 45;
			sampleNum7 /= 0;
			console.log(sampleNum7);//Infinity. This is invalid division because we should not divide by 0.

	//Order of Operations follow (MDAS)

		let mdasResult = 1 + 2 - 3 * 4 / 5;
		console.log(mdasResult);
		/*
			M - Multiplication - 3*4 = 12
			D - Division - 12/5 = 2.4
			A - Addition - 1+2 = 3
			S - Subtraction - 3-2.4 = 0.6 
		*/

		//PEMDAS = Parenthesis, Exponents, Multiplication, Division, Addition, Subtraction

		let pemdasResult = 1 + (2-3) * (4/5);
		console.log(pemdasResult);//0.2
		/*
			P - Parethesis - 4/5 = 0.8
						   - 2-3 = -1
			E
			M - -1 * 0.8 = -0.8
			D
			A - 1 + -0.8 = 0.2
			S
		*/

	//Increment and Decrement

		//Incrementation and Decrementation is adding or subtracting 1 from a variable and then re-assigning the result to the variable.

		//There are 2 implementations of this: Pre-fix and Post-fix

		let z = 1;

		//Pre-fix:

		++z;
		console.log(z);//2 - The value of z was added with 1 and it is immediately returned. With pre-fix increment/decrement, the incremented/decremented value is returned at once.

		//Post-fix:

		console.log(z++);//2
		console.log(z);//3
		//With post-fix incrementation, we add 1 to the value of the variable. However, the difference is that the previous value is returned first before the incremented value.
		z++;//incrementation
		console.log(z);//incremented value to 4

		//Pre-fix vs Post-fix
		console.log(++z);//5 increment first and then returned the incremented value.
		console.log(z++);//5 previous value is returned first before incrementation.
		console.log(z)//6 The new value is now returned.

		//Pre-fix and Post-fix Decrementation

		console.log(--z);//5 - with prefix decrementation the result of subtraction by 1 is returned immediately.
		console.log(z--);//5 - with postfix decrementation the result of subtraction by is is not immediately returned, instead the previous value will be returned.
		console.log(z);//4 - the new value is returned after postfix decrementation.

		//Can we increment or decrement raw number data?

			//incrementation/decrementation are used on variables that contain number types.

			/*console.log(5++);//error*/

		//Can we increment or decrement a string?

			let sampleNumString = "5";
			console.log(++sampleNumString);//6 - a numeric string is converted to number and will be incremented.

			let sampleString = "James";
			console.log(sampleString++);//NaN - incrementation/decrementation will convert your string first into a number, however, since the string contains alphanumeric characters, it converted to NaN.

		//Comparison Operators

			//are used to compare the values of the left and right operands.
			//return a boolean (true/false)

				//Loose Equality Operator

					//Loose equality operator evaluated if the operands have the same value.

					console.log(1 == 1);

					//We can also save the result of comparison in a variable.

					let isSame = 55 == 55;
					console.log(isSame);

					console.log(1 == "1");//true - loose equality operator prioritizes the sameness of the value because loose equality operator actually enforces Forced Coercion or when the types of the operands are automatically changed before the comparison. The string here was converted to number. 1 == 1 = true.

					console.log( 0 == false);//true - with forced coercion, since the operands have different types, both were converted to number, 0 is already a number but false is a boolean, the boolean false when converted to a number is equal to 0. And thus, 0 == 0 = true.

					//JS has a function/method which will allow us to convert data from one type to number.

					let sampleConvert = Number(false);
					console.log(sampleConvert);//0 with the use of the Number() method we can change the type of data to a number type.

					let sampleConvert2 = "2500";
					sampleConvert2 = Number(sampleConvert2);
					console.log(sampleConvert2);//2500 - changed to number type.

					console.log(1 == true);//true - the boolean true when converted to number is 1. So 1 == 1 = true.

					console.log(5 == "5");//true - same value;different type, with loose equality, JS does forced coercion before evaluation.

					console.log(true == "true");//false - forced coercion wherein both operands are converted to numbers. boolean true was converted to a number as 1. string "true" was converted to a number but since it was a work/alphanumeric it resulted to NaN.
					//1 == NaN is false.

					console.log(false == "false");//false - forced coercion: boolean false converted to 0, string "false" converted to NaN.
					//0 == NaN is false.

				//Strict Equality Operator

					//Strict Equality Operator evaluates the sameness of both values and types of operands. Thus, Strict Equality Operator is more preferred because JS is a loose-typed language.

					console.log(1 == "1");// In loose equality this is true.
					console.log(1 === "1");//false - checks sameness of both values and types of the operands.

					console.log("james2000" === "James2000");//false = j is not equal to J

					console.log(55 === "55");//false - strict equality operator (===) checks sameness of value AND type.

				//Loose Inequality Operator

					//Checks whether the operands are NOT equal and/or have different values.
					//Much like Loose Equality Operator, Loose Inequality Operator also does forced coercion.

					console.log(1 != "1");//false
					//Forced Coercion:
					//both operands converted to number:
					//string "1" is also converted to a number.
					//1 != 1 = equal so: not inequal = false.

					/*Loose Inequality Operator returns true if the operands are NOT equal, it will return false if the operands are found to be equal.*/

					console.log("James" != "John");//true - "James" is not equal to "John".

					console.log(5 != 55);//true - 5 is NOT equal to 55.
					console.log(1500 != "5000");//true
					//Forced Coercion:
					//1500 will be converted to number
					//"5000" will be converted to number
					//1500 != 5000 
					//It is NOT equal.

					console.log(true != "true");//true
					//with forced coercion: true was converted to 1
					//"true" was converted to number but results to NaN
					//1 is NOT equal to NaN
					//It is inequal.

				//Strict Inequality Operator

					//Strict Inequality Operator will check whether the two operands have inequal type or value.

					console.log(5 !== 5);
					//false - operands have equal value and same type.
					console.log(5 !== "5");
					//true - operands have same value they have different types.
					console.log(true !== "true");
					//true - operands are inequal because they have different types.

				//Equality Operators and Inequality Operators with Variables.

					let nameStr1 = "Juan";
					let nameStr2 = "Jack";
					let numberSample = 50;
					let numberSample2 = 60;
					let numStr1 = "15";
					let numStr2 = "25";

					console.log(numStr1 == 50);//false
					console.log(numStr1 == 15);//true
					console.log(numStr2 === 25);//false
					console.log(nameStr1 != "James");//true
					console.log(numberSample !== "50");//true
					console.log(numberSample != "50");//false
					console.log(nameStr1 == nameStr2);//false
					console.log(nameStr2 === "jack");//false

		//Relational Comparison Operators

			//A comparison operator which will check the relationship between operands and return boolean.

			let price1 = 500;
			let price2 = 700;
			let price3 = 8000;
			let numStrSample = "5500"

			//greather than (>)
			console.log(price1 > price2);//false - 500 is not greater than 700
			console.log(price3 > price2);//true - 8000 is greater than 700
			console.log(price3 > numStrSample);//true - forced coercion to change the string to a number.

			//less than
			console.log(price2 < price3);//true - 700 is less than 8000
			console.log(price1 < price3);//true - 500 is less than 8000
			console.log(price3 < 1000);//false - 8000 is not less than 1000
			console.log(numStrSample < price1);//false - 5500 is not less than 500

			//greater than or equal to
			console.log(price1 >= 500);//true - 500 is not greater than 500 but is equal.
			console.log(price3 >=10000);//false - 8000 is not greater than or equal to 10000.
			console.log(price2 >= 600);//true - 700 is not equal to but is greater than 600.

			//less than or equal to
			console.log(price2 <= numStrSample);//true - 700 is not equal to but less than 5000.
			console.log(price3 <= price1);//false
			console.log(price2 <= 700);//true - 700 is not less than but equal to 700. 

		//Logical Operators

			//and &&
				//And operator evaluated both the left and right operands and both left and right operands must be true so that the operation would result to true. If at least one operand is false, then, the operation would result to false.

				let isAdmin = false;
				let isRegistered = true;
				let isLegalAge = true;

				console.log(isRegistered && isLegalAge);//true - because both left and operand result to true.
				console.log(isAdmin && isRegistered);//false - at least one of the operands resulted to false.

				let user1 = {
					username: "peterphoenix_1999",
					age: 28,
					level: 15,
					isGuildAdmin: false
				}

				let user2 = {
					username: "kingBrodie00",
					age: 13,
					level: 50,
					isGuildAdmin: true
				}

				//To be able to access the properties of an object we use dot notation:
				console.log(user1.username);//peterphoenix_1999
				console.log(user1.level);//15

				//Authorization
				let authorization1 = user1.age >= 18 && user1.level >=25;
				console.log(authorization1);//false

				let authorization2 = user2.age >=18 && user2.level >=25;
				console.log(authorization2);//false

				//Guild Leaders/Admin Meeting
				let authorization3 = user1.level >=10 && user1.isGuildAdmin === true;
				console.log(authorization3);//false

				let authorization4 = user2.level >=10 && user2.isGuildAdmin === true;
				console.log(authorization4);//true

			//or ||

				//Or operator returs true if at least one operand results to true.

				/*
					let user1 = {
						username: "peterphoenix_1999",
						age: 28,
						level: 15,
						isGuildAdmin: false
					}

					let user2 = {
						username: "kingBrodie00",
						age: 13,
						level: 50,
						isGuildAdmin: true
					}
				*/

				//On-site meeting between new members
				let authorization5 = user1.age >= 18 || user1.level <= 15;
				console.log(authorization5);//true

				//Zoom meeting between members
				let authorization6 = user2.age >=18 || user2.level >= 1;
				console.log(authorization6);//true

				//Joining a new group with low levels
				let authorization7 = user1.level >= 10 || user1.isGuildAdmin === false;
				console.log(authorization7);//true

				//Joining a new group 
				let authorization8 = user1.level >= 50 || user1.isGuildAdmin === false;
				console.log(authorization8);//true

			//not !

				//turns a boolean into the opposite value

				/*
					let isAdmin = false;
					let isRegistered = true;
					let isLegalAge = true;
				*/

				console.log(!isRegistered);//false
				console.log(!isAdmin);//true

		//Conditional Statements

			//A conditional statement is a key feature of a programming language.
			//It allows us to perform certain tasks based on a condition.
			//Sample Conditional Statements:
				//Is it windy today? (What do we do?)
				//Is it Monday today? (What do we do?)

		//If-Else Statements

			//If statement will run a code block is the condition specified is true or results to true.

			/*
				let user1 = {
					username: "peterphoenix_1999",
					age: 28,
					level: 15,
					isGuildAdmin: false
				}

				let user2 = {
					username: "kingBrodie00",
					age: 13,
					level: 50,
					isGuildAdmin: true
				}
			*/

			if(user1.age >= 18){
				alert("You are allowed to enter!")
			}

			/*
				The code block of an if condition is run only when the condition given is met or results to true.

				syntax:

				if(true){
					//code block to run
				}

				else statement will be run if the given condition in the if statement is not met.

				if(true){
					//code block to run
				} else {
					//code block to run if if condition is not met.
				}
			*/

			if(user2.age >= 18){
				alert("User2 you are allowede to enter.")
			} else {
				alert("User2, you are not allowede to enter.")
			}

			if(user1.level >= 20){
				console.log("User1 is not a noobie.")
			} else {
				console.log("User1 is a noobie.")
			}

			if(user2.level >= 20){
				console.log("User2 is not a noobie.")
			} else {
				console.log("User2 is a noobie.")
			}

			/*Mini-Activity (if-else)*/

			if(user1.isGuildAdmin === true){
				console.log("Welcome Back, Guild Admin")
			} else {
				console.log("You are not authorized to enter.")
			}

			/*else if*/

			//else if executes a code block if the previous/original condition is not met but we have met another specified condition.

			//if a statemnet has both else-if and else, else will run if all conditions in the if or else-if are not met.

			if(user1.level >= 35){
				console.log("Hello, Knight!")
			} else if(user1.level >= 25){
				console.log("Hello, Swordsman!")
			} else if(user1.level >= 10){
				console.log("Hello, Rookie!")
			} else {
				console.log("Level out of range")
			}

			if(user2.level >= 35){
				console.log("Hello, Knight!")
			} else if(user2.level >= 25){
				console.log("Hello, Swordsman!")
			} else if(user2.level >= 10){
				console.log("Hello, Rookie!")
			} else {
				console.log("Level out of range")
			}

			//Logical Operators for If conditions:

			let usernameInput1 = "nicoleIsAwesome100";
			let passwordInput1 = "iamawesomenicole";

			let usernameInput2 = ""; 
			let passwordInput2 = null;

			function register(username,password){
				if(username === "" || password === null){
					console.log("Please complete the form.")
				} else {
					console.log("Thank you for registering.")
				}
			}

			register(usernameInput1,passwordInput1);
			register(usernameInput2,passwordInput1);

			function requirementChecker(level,isGuildAdmin){
				if(level <= 25 && isGuildAdmin === false){
					console.log("Welcome the the Newbies Guild.")
				} else if(level > 25){
					console.log("You are too strong.")
				} else if(isGuildAdmin === true){
					console.log("You are a guild admin.")
				} 
			}

			requirementChecker(user1.level,user1.isGuildAdmin);//can join
			requirementChecker(user2.level,user2.isGuildAdmin);//cannot join

			let user3 = {
				username: "richiBillions",
				age: 20,
				level: 20,
				isGuildAdmin: true
			}

			requirementChecker(user3.level,user3.isGuildAdmin);//cannot join

			function addNum(num1,num2){
				//Check if the numbers being passed are both number types.
				//typeof keyword returns a string which tells the type of data that follows it
				if(typeof num1 === "number" && typeof num2 === "number"){
					console.log("Run only if both arguments passed are number types.")
				} else {
					console.log("One or both of the arguments are not numbers.")
				}
			}

			addNum(5,10);//run the if statement because both arguments are numbers.
			addNum("6",20);//run the else because one of the arguments is not a number.

			//typeof - used for validating the data type of variables or data.
			//It returns a string after evaluating the data type of the data/variable that comes after it.

			let str = "sample"
			console.log(typeof str);
			console.log(typeof 75);

			/*Mini-Activity*/

			function dayChecker(day){

				//toLowercase() method makes a string all small caps.
				/*day = day.toLowerCase();
				console.log(day);

				if (day === "sunday"){
					console.log("Today is Sunday; Wear White.")
				} else if(day === "monday"){
					console.log("Today is Monday; Wear Blue")
				} else if(day === "tuesday"){
					console.log("Today is Tuesday; Wear Green")
				} else if(day === "wednesday"){
					console.log("Today is Wednesday; Wear Purple")
				} else if(day === "thursday"){
					console.log("Today is Thursday; Wear Brown")
				} else if(day === "friday"){
					console.log("Today is Friday; Wear Red")
				} else if(day === "saturday"){
					console.log("Today is Saturday; Wear Pink")
				}*/

				/*
					Switch is a conditional statement which can be an alternative to else-if, if-else, where the data being checked or evaluated is of an expected input.

					Switch will compare your expression/condition to match with a case. Then the statement for that case will run.

					syntax:

						switch(expression/condition){
							case value:
								statement;
								break;
							default;
								statement;
								break;
						}
				*/

				switch(day.toLowerCase()){
					case "sunday":
					console.log("Today is " + day + "; Wear White");
						break;
					case "monday":
					console.log("Today is " + day + "; Wear Blue");
						break;
					case "tuesday":
					console.log("Today is " + day + "; Wear Green");
						break;
					case "wednesday":
					console.log("Today is " + day + "; Wear Purple");
						break;
					case "thursday":
					console.log("Today is " + day + "; Wear Brown");
						break;
					case "friday":
					console.log("Today is " + day + "; Wear Red");
						break;
					case "saturday":
					console.log("Today is " + day + "; Wear Pink");
						break;
					default:
					console.log("Invalid Input. Enter a valid day of the week.")
				}
			}

			dayChecker("Wednesday");


			//Eugene,Vincent,Dennis,Alfred,Jeremiah

			//A switch which will display the power level of a selected member

			let member = "Jeremiah"

			//the break statement is used to terminate the execution of the switch after it found a match.

			switch(member){
				case "Eugene":
					console.log("Your power level is 20000");
					break;
				case "Dennis":
					console.log("Your power level is 15000");
					break;
				case "Vincent":
					console.log("Your power level is 14500");
					break;
				case "Jeremiah":
					console.log("Your power level is 10000");
					break;
				case "Alfred":
					console.log("Your power level is 8000");
					break;
				default:
					console.log("Invalid Input. Add a Ghost Fighter member.")
			}

			/*Mini-Activity*/

			function capitalChecker(country){
				switch(country.toLowerCase()){
					case "philippines":
						console.log("Manila");
						break;
					case "usa":
						console.log("Washington D.C.");
						break;
					case "japan":
						console.log("Tokyo");
						break;
					case "germany":
						console.log("Berlin");
						break;
					default:
						console.log("Input is out of range. Choose another country.");
				}
			}

			capitalChecker("Philippines");
			
			//Ternary Operator

				//Conditional statement as if-else. However, it was introduced to be a short-hand way to write if-else.

				/*
					Syntax

					condition ? if-statement : else-statement
				*/

				let superHero = "Batman"

				superHero === "Batman" ? console.log("You are rich.") : console.log("Bruce Wayne is richer than you.");

				//Ternary operators require an else statement
				/*superHero ==="Superman" ? console.log("Hi, Clark!");
*/
				//Ternary operators can implicitly return value even without the return keyword.

				let currentRobin = "Tim Drake";

				let isFirstRobin = currentRobin === "Dick Grayson" ? true : false
				 console.log(isFirstRobin);


			/*Exercise 1: login()*/

				function login(username,password){
					if (typeof username === "string" && typeof password === "string"){
						console.log("Both Arguments Are Strings.")
					} else {
						console.log("One of the arguments is not a string.")
					}
				}

				login("user","pass");
				login("user",45);

			/*Exercise 2: oddEvenChecker()*/

				function oddEvenChecker(num){
					if (num % 2 === 0){
						console.log("The number is even.")
					} else {
						console.log("The number is odd.")
					}
				}

				oddEvenChecker(2);
				oddEvenChecker(3);

			/*Exercise 3: budgetChecker()*/

				function budgetChecker(num){
					if (num > 40000){
						console.log("You are over the budget.")
					} else {
						console.log("You have resources left.")
					}
				}

				budgetChecker(20);
				budgetChecker(50000);

				/*try-catch-finally statement*/
				/*
					- "try catch" statements are commonly used for error handling
					- there are instances when the application returns an error/warning that is not necessarily an error in the context of our code.
					- these errors are a result of an attempt of the programming lanuage to help developers in creating efficient code.
					- they are used to specify a response whenever an exception/error is received.
					- it is also useful for debugging code because of the "error" object that can be "caught" when using the try catch statement.
					- In programming languages, an "error" object is used to provide detailed information about an error and which also provides access to functions that can be used to handle/resolve errors to create "exceptions" within our code.
					- The "finally" block is used to specify a response/action that is used to handle/resolve errors.
				*/

				/*function showIntensityAlert(windSpeed){
					try {

						//Attempt to execute a code
						alert(determineTyphoonIntensity(windSpeed));

						//error/err are commonly used variable names used by the developers for storing errors.
					} catch (error) {

						//"typeof" operator is used to check the data type of a value/expression and returns a string value of what the data type is. 
						console.log(typeof error);

						//catch errors within "try" statement
						//in this case, the error is an unknown function "alert" which does not exist in Javascript
						//the 'alert' function is used similarly to a prompt to alert the user
						//'error.message' is used to access the information relating to an error object.
						console.log(error.message);
					} finally {
						//continue execution of code regardless of success and failure of code execution in the 'try' block to handle/resolve errors.
						alert("Intensity updates will show new alert");
					}
				}

				showIntensityAlert(56);*/

				/*const num = 100, x = "a";

				try {
					console.log(num/x);
					console.log(a);
				} catch (error) {
					console.log("An error caught.");
					console.log("Error message: " + error);
				} finally {
					alert("Finally will execute.");
				}*/


				/*Mini-Activity (try-catch-finally)*/

				/*function gradeEvaluator(grade){
					try {
						if (grade >= 90){
							gradeMsg = "A";
						} else if (grade >= 80) {
							gradeMsg = "B";
						} else if (grade >= 71) {
							gradeMsg = "C";
						} else if (grade <= 70){
							gradeMsg = "F";
						} else if (typeof grade !== "number"){
							throw Error("Not a number");
						}
						console.log("Your Grade:" + gradeMsg)
					} catch (error) {
						console.log(error);
					} finally {
						alert("Message.");
					}
				}

				gradeEvaluator(91);
				gradeEvaluator(85);
				gradeEvaluator(73);
				gradeEvaluator(67);
				gradeEvaluator("D");*/