let user1 = {
	username: "jacksepticeye",
	email: "seanScepticeye@gmail.com",
	age: 31,
	topics: ["Gaming", "Commentary"]
}

console.log(user1);

//First Function
function isLegalAge(object){
	if (object.age >= 18){
		console.log("Welcome to the Club.")
	} else {
		console.log("You cannot enter the club yet.")
	}
}

isLegalAge(user1);

//Second Function
function addTopic(string){
	if (string.length >= 5){
		user1.topics.push(string);
		console.log(string + " was added to topics.")
	} else {
		console.log("Enter a topic with at least 5 characters.")
	}
}

addTopic("Math");
addTopic("Comedy");
console.log(user1.topics);

let clubMembers = ["jacksepticeye", "pewdiepie"];

//Third Function
function register(member){
	if (member.length >= 8){
		clubMembers.push(member);
		console.log(member + " was added to the club.")
	} else {
		console.log("Please enter a username longer or equal to 8 characters.")
	}
}

register("myname");
register("markiplier");
console.log(clubMembers);